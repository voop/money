<?php namespace Voop\Money;

/**
 * Class NumFormat
 *
 * @package Voop\Money
 */
class NumFormat
{
    /** @const float + разделить пробелом тысячи: 123 123.45 */
    const FLOAT_SEP_SPACE = 1;

    /** @const int + разделить пробелом тысячи: 123 123 */
    const INT_SEP_SPACE = 2;

    /** @const float без разделения тысячь: 123123.45 */
    const FLOAT_SEP_NONE = 3;

    /** @const int без разделения тысячь: 12365456 */
    const INT_SEP_NONE = 4;

    /** @const money-string + разделить пробелом тысячи: 123 123,45 */
    const MONEY_SEP_SPACE = 5;

    /** @const money-string без разделения тысячь: 123123,45 | 123123,00 */
    const MONEY_SEP_NONE = 6;


    /**
     * @param mixed $value - число
     * @param int   $type  - формат отображения
     * @return string
     */
    public function format($value, int $type = self::FLOAT_SEP_NONE): string
    {
        // null не форматируется!
        if (is_null($value)) {
            return '';
        }
        switch ($type) {
            case self::FLOAT_SEP_SPACE:
                $dec = 2;
                $sep = ' ';
                $point = '.';
                break;
            case self::INT_SEP_SPACE:
                $dec = 0;
                $sep = ' ';
                $point = '';
                break;
            case self::FLOAT_SEP_NONE:
                $dec = 2;
                $sep = '';
                $point = '.';
                break;
            case self::INT_SEP_NONE:
                $dec = 0;
                $sep = '';
                $point = '';
                break;
            case self::MONEY_SEP_SPACE:
                $dec = 2;
                $sep = ' ';
                $point = ',';
                break;
            case self::MONEY_SEP_NONE:
                $dec = 2;
                $sep = '';
                $point = ',';
                break;
            default:
                $dec = 2;
                $sep = '';
                $point = '.';
        }

        return number_format($value, $dec, $point, $sep);
    }
}
