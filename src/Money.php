<?php namespace Voop\Money;

/**
 * Класс для работы с денежными единицами. Хранит сумму в int-копейках.
 * И следовательно избавляет от необходимости работать с float
 *
 * @package Voop\Money
 * @see \Voop\Money\Test\MoneyTest
 */
class Money
{
    /** коэффициент для преобразования рублей в копейки и наоборот  */
    const RATE = 100;

    /**
     * @var int|null
     */
    private $value;

    /**
     * @var NumFormat
     */
    private $formatter;


    /**
     * 1.Сюда пихать сразу integer сумму в КОПЕЙКАХ преобразования нет
     * 2.Если нужно создавать объект из некой сумму "на лету" надо юзать так - Money::build(123.55)
     * 3.Отрицательные числа на входе разрешены
     * 4.(!)Объявлять тип входящих данных ( __construct( INT $value) ) нельзя -
     * будет конвертить float к int (123.45 -> 123) поэтому надо пользовать ТОЛЬКО проверку !is_int($value) и Exception
     *
     * @param int|null $value - сумма в КОПЕЙКАХ
     */
    public function __construct($value = null)
    {
        // чтоб не напихали шлак
        if (!is_int($value) && !is_null($value)) {
            throw new \InvalidArgumentException(sprintf('Только int значения! Передано "%s" тип "%s"', $value, gettype($value)));
        }
        $this->formatter = new NumFormat;
        $this->value = $value;
    }

    /**
     * @return int|null
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * @param Money $money
     * @return Money
     */
    public function subtract(Money $money): Money
    {
        return new Money($this->value - $money->getValue());
    }

    /**
     * @param Money $money
     * @return Money
     */
    public function addition(Money $money): Money
    {
        return new Money($this->value + $money->getValue());
    }

    /**
     * @param int $factor
     * @return Money
     */
    public function multiply(int $factor): Money
    {
        return new Money(is_null($this->value) ? null : $this->value * $factor);
    }


    /**
     * @return float|null
     */
    public function getAsFloat()
    {
        if (is_null($this->value)) {
            return null;
        }
        return $this->value / self::RATE;
    }


    /**
     * @param int $type - формат отображения
     * @return string
     */
    public function format(int $type = NumFormat::FLOAT_SEP_NONE): string
    {
        return $this->formatter->format($this->getAsFloat(), $type);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->format();
    }


    /**
     *  Конвертит входные параметры через self::_convert() и возвращает новый объект Money
     *  Вернет false если передали число в неправильном формате или не число (@see self::_convert())
     *
     * @param mixed $value
     * @return bool|Money
     */
    public static function build($value)
    {
        if (($value = self::_convert($value)) !== false) {
            return new Money($value);
        }

        return false;
    }


    /**
     * Проверяет значения на валидность и если значение валидное
     * преобразовывает его к копейкам т.е. уможат на self::RATE
     * Тоесть воспринимает ЛЮБОЕ число как РУБЛИ которые нужно привести к копейкам
     * Если есть дробная часть то воспринимает его как копейки
     * Если надо сразу в копейках юзать так: new Money(12300) где 12300 - сумма в копейках
     *
     * @param mixed $value
     * @return bool|int
     */
    private static function _convert($value)
    {
        // int
        if (is_int($value)) {
            return $value * self::RATE;
        }

        if (is_null($value)) {
            return null;
        }

        if (is_float($value)) {
            $value = (string)$value;
        }

        // неправильные данные
        if (!is_string($value) || !is_numeric($value)) {
            return false;
        }

        /**
         * После "запятой" можно от 0 до 2 знаков (123.4 и 123.45 - можно, а 123.455 - не пройдет)
         * т.к. 123.455 * 100 = 12345.5 - пол-копейки не бывает
         */
        if (!preg_match('/^-?\d+(\.\d{1,2})?$/', $value)) {
            return false;
        }

        // сначала еще раз приводим к (string) - нельзя конвертить float сразу к int`у
        return (int)(string)($value * self::RATE);
    }


    /**
     * Множественное сложение
     *
     * @param Money[] $moneyArr
     * @throws \InvalidArgumentException
     * @return Money
     */
    public static function collectiveAddition(array $moneyArr): Money
    {
        $baseMoney = new Money;
        foreach ($moneyArr as $money) {
            if (!is_object($money) || !($money instanceof Money)) {
                throw new \InvalidArgumentException('Передан неверный параметр для множественного сложения');
            }
            $baseMoney = $baseMoney->addition($money);
        }

        return $baseMoney;
    }
}
