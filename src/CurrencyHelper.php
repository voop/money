<?php namespace Voop\Money;

/**
 * Чисто помогает вывести строкой название переданного кода валюты
 *
 * @package Voop\Money
 * Class CurrencyHelper
 */
class CurrencyHelper
{
    /**
     * Ключ текущей валюты
     *
     * @var string
     */
    private $currency;


    /**
     * @param null|string $currency
     */
    public function __construct(string $currency = null)
    {
        $this->currency= $currency;
    }

    /**
     * Билдит сумму с валютой "123,54 руб." или "132,45 тенге."
     *
     * @param Money $money
     * @return string
     */
    public function buildNumWithCurrency(Money $money): string
    {
        if (! $currency = $this->currency) {
            return $money->format(NumFormat::MONEY_SEP_SPACE);
        }

        return sprintf('%s %s', $money->format(NumFormat::MONEY_SEP_SPACE), $this->currency ?? "");
    }
}
