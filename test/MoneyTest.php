<?php namespace Voop\Money\Test;

use PHPUnit\Framework\TestCase;
use Voop\Money\NumFormat;
use Voop\Money\Money;

/**
 * @see  \Voop\Money\Money
 */
class MoneyTest extends TestCase
{
    /**
     * Тест базовой инициализации
     */
    public function testMoneyBase()
    {
        $money = new Money(null);
        $this->assertNull($money->getValue());

        $money = new Money(123);
        $this->assertSame($money->getValue(), 123);

        $money = new Money(-123);
        $this->assertSame($money->getValue(), -123);
    }


    /**
     * Тест форматирования данных
     */
    public function testFormat()
    {
        $money = new Money(123);
        // преобразования к float
        $this->assertSame($money->getAsFloat(), 1.23);

        // tostring
        $this->assertSame((string)$money, '1.23');

        // отображение
        $money = new Money(500000);
        $this->assertSame('5 000.00', $money->format(NumFormat::FLOAT_SEP_SPACE), 'формат c копейками разделитель пробел');
        $this->assertSame('5 000', $money->format(NumFormat::INT_SEP_SPACE), 'формат без копеек разделитель пробел');
        $this->assertSame('5000.00', $money->format(NumFormat::FLOAT_SEP_NONE), 'формат с копейками без разделителя');
        $this->assertSame('5000', $money->format(NumFormat::INT_SEP_NONE), 'формат без копеек без разделителя');


        $money = new Money(null);
        $this->assertSame('', $money->format(), 'null никак не отформатируется');
    }


    /**
     * Проверка конвертации входных парметров в методе-билдере
     */
    public function testBuilder()
    {
        $params = [
            [null,      new Money(null)],
            [0,         new Money(0)],
            [123,       new Money(12300)],
            [123.45,    new Money(12345)],
            [123.4,     new Money(12340)],
            [19.99,     new Money(1999)], /** @see http://php.net/manual/es/function.intval.php#101439 */
            [123.451,   false],
            ['123.451', false],
            ['123.45',  new Money(12345)],
            ['123.4',   new Money(12340)],
            ['123',     new Money(12300)],
            ['0',       new Money(0)],

            [-123,      new Money(-12300)],
            [-123.45,   new Money(-12345)],
            ['-123.45', new Money(-12345)],
            ['-123',    new Money(-12300)],

            [false,     false],
            [true,      false],
            ['123.45f', false],
            ['123.45 ', false],
            ['2 123.4', false],
            ['2123,42', false],
            ['2123,4',  false],
            ['2123,',   false],
            ['2123.',   false],
            ['2123.d',  false],
            ['2123.dd', false],
            ['2123,dd', false],
        ];

        foreach ($params as list($input, $expected)) {
            $result = Money::build($input);
            if ($result instanceof Money) {
                $this->assertSame($expected->getValue(), $result->getValue());
            } else {
                $this->assertSame($expected, $result);
            }
        }
    }


    /**
     * Попытка передать невалидные значения
     *
     * @param mixed $value
     * @dataProvider dataProviderForInvalidValue
     */
    public function testInvalidValue($value)
    {
        $this->expectException(\InvalidArgumentException::class);
        new Money($value);
    }


    /**
     * @return array
     */
    public function dataProviderForInvalidValue()
    {
        return [
            [123.45],
            ['123.45'],
            ['123,45'],
            ['123000'],
            [123.0],
            [false],
        ];
    }


    /**
     * Сложение/вычитание
     */
    public function testAdditionSubtraction()
    {
        // ["0" - операнд1, "1" - операнд2, "2" - результат сложения, "3" - результат вычитания]
        $numsForExpressions = [
            [-1, -1, -2, 0],
            [-10, -5, -15, -5],
            [-5, -10, -15, 5],
            [0, -5, -5, 5],
            [-5, 0, -5, -5],

            [5, 0, 5, 5],
            [0, 5, 5, -5],
            [5, 5, 10, 0],
            [10, 5, 15, 5],
            [5, 10, 15, -5],

            [5, null, 5, 5],
            [null, 5, 5, -5],
            [-5, null, -5, -5],
            [null, -5, -5, 5],
        ];

        foreach ($numsForExpressions as list($operand1, $operand2, $additionalResult, $substractResult)) {

            $money = new Money($operand1);
            $this->assertEquals($money->subtract(new Money($operand2)), new Money($substractResult), sprintf('вычитание: %s - %s', $operand1, $operand2));


            $money = new Money($operand1);
            $this->assertEquals($money->addition(new Money($operand2)), new Money($additionalResult), sprintf('сложение: %s + %s', $operand1, $operand2));
        }
    }


    /**
     * Сложение/вычитание
     */
    public function testMultiply()
    {
        // ["0" - значение объекта-операнда, "1" - множитель, "2" - результат]
        $numsForExpressions = [
            [-1, -1, 1],
            [-10, -5, 50],
            [-5, -10, 50],
            [0, -5, 0],
            [-5, 0, 0],
            [5, 0, 0],
            [0, 5, 0],
            [5, 5, 25],
            [10, 5, 50],
            [5, 10, 50],
            [null, 5, null],
            [null, -5, null],
        ];

        foreach ($numsForExpressions as list($value, $factor, $result)) {
            $money = new Money($value);
            $this->assertEquals($money->multiply($factor)->getValue(), $result, sprintf('умножение: %s + %s', $value, $factor));
        }
    }





    /**
     * Сложение/вычитание float
     */
    public function testAdditionSubtractionWithDouble()
    {
        // ["0" - операнд1, "1" - операнд2, "2" - результат сложения, "3" - результат вычитания]
        $numsForExpressions = [
            [5.5, 4.4, 990, 110],
            [21.00, 1.01, 2201, 1999], /** @see 19.99 http://php.net/manual/es/function.intval.php#101439 */
            [18.98, 1.01, 1999, 1797], /** @see 19.99 http://php.net/manual/es/function.intval.php#101439 */
        ];

        foreach ($numsForExpressions as list($operand1, $operand2, $additionalResult, $substractResult)) {

            $operand1 = (int)($operand1 * 100);
            $operand2 = (int)($operand2 * 100);

            $money = new Money($operand1);
            $this->assertEquals($money->subtract(new Money($operand2))->getValue(), $substractResult, sprintf('вычитание float: %s - %s', $operand1, $operand2));


            $money = new Money($operand1);
            $this->assertEquals($money->addition(new Money($operand2))->getValue(), $additionalResult, sprintf('сложение float: %s + %s', $operand1, $operand2));
        }
    }


    /**
     * Множественное сложение
     */
    public function testCollectiveAddition()
    {
        $values = [
            0 => [new Money(1), new Money(2), new Money(3)],
            1 => [new Money, new Money],
            2 => [new Money(0), new Money(null)]
        ];

        // множественное сложение
        $this->assertEquals(6, Money::collectiveAddition($values[0])->getValue(), 'множественное сложение');
        $this->assertEquals(null, Money::collectiveAddition($values[1])->getValue(), 'множественное сложение null');
        $this->assertEquals(0, Money::collectiveAddition($values[2])->getValue(), 'множественное сложение 0');
    }
}
