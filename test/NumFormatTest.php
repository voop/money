<?php namespace Voop\Money\Test;

use PHPUnit\Framework\TestCase;
use Voop\Money\NumFormat;

class NumFormatTest extends TestCase
{
    /**
     * Тест форматирования данных
     */
    public function testFormat()
    {
        $format = new NumFormat();
        $this->assertSame('5 000.00', $format->format(5000, NumFormat::FLOAT_SEP_SPACE), 'формат c копейками разделитель пробел');
        $this->assertSame('5 000', $format->format(5000, NumFormat::INT_SEP_SPACE), 'формат без копеек разделитель пробел');
        $this->assertSame('5000.00', $format->format(5000, NumFormat::FLOAT_SEP_NONE), 'формат с копейками без разделителя');
        $this->assertSame('5000.25', $format->format(5000.25, NumFormat::FLOAT_SEP_NONE), 'формат с копейками без разделителя');
        $this->assertSame('5000', $format->format(5000, NumFormat::INT_SEP_NONE), 'формат без копеек без разделителя');

        $this->assertSame('5 000,00', $format->format(5000, NumFormat::MONEY_SEP_SPACE), 'формат c копейками, дробная часть отделена ЗАПЯТОЙ. разделитель тысяч пробел');
        $this->assertSame('5000,00', $format->format(5000, NumFormat::MONEY_SEP_NONE), 'формат c копейками, дробная часть отделена ЗАПЯТОЙ. без разделения тысяч');

        $this->assertSame('', $format->format(null, NumFormat::FLOAT_SEP_SPACE), 'null никак не отформатируется');
    }
}
